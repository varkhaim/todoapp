package io.github.mat.todoapp.controller;

import io.github.mat.todoapp.model.Task;
import io.github.mat.todoapp.model.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
class TaskController {
    private static final Logger logger = LoggerFactory.getLogger(TaskController.class);
    private final TaskRepository repository;

    TaskController(final TaskRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/tasks", params = {"!sort", "!page", "!size"})
    ResponseEntity<?> readAllTasks()
    {
        logger.warn("Exposing all the tasks!");
        return ResponseEntity.ok(repository.findAll());
    }

    @GetMapping(value = "/tasks")
    ResponseEntity<?> readAllTasks(Pageable page)
    {
        logger.info("Custom pager");
        return ResponseEntity.ok(repository.findAll(page));
    }

    @PutMapping("/tasks/{id}")
    ResponseEntity<?> updateTask(@PathVariable("id") int id, @RequestBody @Valid Task toUpdate)
    {
        if (!repository.existsById(id))
        {
            return ResponseEntity.notFound().build();
        }
        ((TaskRepository)repository).findById(id).ifPresent(task -> {
            task.updateFrom(toUpdate);
            repository.save(task);
        });
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/tasks/{id}")
    ResponseEntity<?> readTask(@PathVariable("id") int id)
    {
        if (!repository.existsById(id))
        {
            return ResponseEntity.notFound().build();
        }
        Optional<Task> task = ((TaskRepository) repository).findById(id);
        return ResponseEntity.ok(task);
    }

    @PostMapping("/tasks")
    ResponseEntity<?> createTask(@RequestBody @Valid Task toCreate)
    {
        Task newTask = ((TaskRepository)repository).save(toCreate);

        if (newTask == null)
        {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.created(URI.create("/" + newTask.getId())).body(newTask);
    }

    @Transactional
    @PatchMapping("/tasks/{id}")
    public ResponseEntity<?> toggleTask(@PathVariable int id)
    {
        if (!repository.existsById(id))
        {
            return ResponseEntity.notFound().build();
        }
        ((TaskRepository)repository).findById(id).ifPresent(task -> task.setDone(!task.isDone()));
        return ResponseEntity.noContent().build();
    }
}
