package io.github.mat.todoapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@Table(name = "tasks")
public class Task
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotBlank(message = "Task description must not be null")
    private String description;
    private boolean done;

    public LocalDateTime getDeadline() {
        return deadline;
    }

    private LocalDateTime deadline;
    @Embedded
    private Audit audit = new Audit();

    Audit getAudit() {
        return audit;
    }

    void setAudit(Audit audit) {
        this.audit = audit;
    }

    TaskGroup getGroup() {
        return group;
    }

    void setGroup(TaskGroup group) {
        this.group = group;
    }

    @ManyToOne
    @JoinColumn(name = "task_group_id")
    private TaskGroup group;

    public Task(@NotBlank(message = "Task description must not be null") String description, LocalDateTime deadline) {
        this.description = description;
        this.deadline = deadline;
    }

    public int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public LocalDateTime getDeadLine() {
        return deadline;
    }

    void setDeadLine(LocalDateTime deadLine) {
        this.deadline = deadLine;
    }

    public void updateFrom(final Task source)
    {
        description = source.description;
        done = source.done;
        deadline = source.deadline;
        group = source.group;
    }
}
